import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/confirm_page.dart';
import 'package:homeservice_ashlyn/profile_page.dart';

class Service_page extends StatelessWidget {
  const Service_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new_outlined,
                    size: 30,
                  ),
                ),
                Spacer(),
                Text("Cleaner's Profile", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                Spacer(),IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.account_circle_outlined,
                    size: 40,
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(250, 151, 252, .7),
                    blurRadius: 5,
                    offset: Offset(4, 6),
                  ),
                ]),
            padding: EdgeInsets.all(12),
            height: MediaQuery.of(context).size.height * .4,
            width: MediaQuery.of(context).size.width * .7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Spacer(),
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 3, color: Colors.grey),
                            borderRadius: BorderRadius.circular(10)),
                        child: Image.asset('assets/ashlyn.png')),
                    Spacer(),
                  ],
                ),
                Row(
                  children: [
                    Spacer(),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    Spacer(),
                  ],
                ),
                Text(
                  "Ashlyn Kate Caburnay",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "21 Years Old",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Manoag, Pangsainan",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "0912-345-6789",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Cleaner",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Spacer(),
                Center(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Confirm_page(),
                        ),
                      );
                    },
                    child: Text(
                      "Choose",
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(250, 151, 252, .7),
                    blurRadius: 5,
                    offset: Offset(4, 6),
                  ),
                ]),
            padding: EdgeInsets.all(12),
            height: MediaQuery.of(context).size.height * .4,
            width: MediaQuery.of(context).size.width * .7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Spacer(),
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 3, color: Colors.grey),
                            borderRadius: BorderRadius.circular(10)),
                        child: Image.asset('assets/claire.png')),
                    Spacer(),
                  ],
                ),
                Row(
                  children: [
                    Spacer(),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    Spacer(),
                  ],
                ),
                Text(
                  "Christine Claire Bergado",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "21 Years Old",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Urdaneta City, Pangasinan",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "0902-0255-0000",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Cleaner",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Spacer(),
                Center(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Confirm_page(),
                        ),
                      );
                    },
                    child: Text(
                      "Choose",
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
