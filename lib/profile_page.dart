import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/dashboard_page.dart';
import 'package:homeservice_ashlyn/login_page.dart';
import 'package:homeservice_ashlyn/rehistro_pahina.dart';

class Profile_page extends StatefulWidget {
  const Profile_page({super.key});

  @override
  State<Profile_page> createState() => _Profile_page();
}

class _Profile_page extends State<Profile_page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back_ios_new_outlined,
                      size: 30,
                    ),
                  ),
                  Spacer(),
                  Text("User's Profile"),
                  Spacer(),
                  Spacer(),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(250, 151, 252, 1),
                ),
                height: MediaQuery.of(context).size.height * .7,
                width: MediaQuery.of(context).size.width * 8,
                child: Center(
                  child: Column(
                    children: [Spacer(),Spacer(),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(width: 3)),
                        child: Image.asset('assets/ashlyn.png'),
                      ),Spacer(),
                      Row(
                        children: [
                          Spacer(),
                          Expanded(
                            flex: 8,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(width: 1)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Bla b. Bla"),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),Spacer(),
                      Row(
                        children: [
                          Spacer(),
                          Expanded(
                            flex: 8,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(width: 1)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Anywhere St."),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),Spacer(),
                      Row(
                        children: [
                          Spacer(),
                          Expanded(
                            flex: 8,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(width: 1)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("0912345678"),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),Spacer(),
                      Row(
                        children: [
                          Spacer(),
                          Expanded(
                            flex: 8,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(width: 1)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("blabla@gmail.com"),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),Spacer(),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: BorderSide(width: 1),
                          ),
                        ),
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Login_page(),),);
                        },
                        child: Text(
                          "Log Out",
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                      Spacer(),Spacer(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
