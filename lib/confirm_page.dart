import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/dashboard_page.dart';

class Confirm_page extends StatelessWidget {
  const Confirm_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Dashboard_page(),
                          ),
                        );
                      },
                      icon: Icon(Icons.arrow_back_ios_new_outlined)),
                ],
              ),
            ),
            Spacer(),
            Container(
              height: MediaQuery.of(context).size.height * .5,
              width: MediaQuery.of(context).size.width * .7,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(250, 151, 252, 1),
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: [
                  Spacer(),
                  Text(
                    "Your",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Schedule is",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "already",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Approved",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Spacer(),
                  Text(
                    "Thank you!",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
