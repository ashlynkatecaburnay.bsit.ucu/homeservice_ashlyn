import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/piliaraw_page.dart';
import 'package:homeservice_ashlyn/profile_page.dart';
import 'package:homeservice_ashlyn/where_daw_page.dart';

class Dashboard_page extends StatelessWidget {
  const Dashboard_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Spacer(),
                Spacer(),
                Text(
                  "Dashboard",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.account_circle_outlined,
                    size: 40,
                  ),
                ),
              ],
            ),
          ),
          Text(
            "Which service do you need?",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Row(
            children: [
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Where_page(),
                    ),
                  );
                },
                child: Container(
                  child: Column(
                    children: [
                      Expanded(flex: 8, child: Image.asset('assets/clean.png')),
                      Text(
                        "Cleaning",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                      Spacer(),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: MediaQuery.of(context).size.width * .3,
                  height: MediaQuery.of(context).size.height * .18,
                ),
              ),
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Schedule_page(),
                    ),
                  );
                },
                child: Container(
                  child: Column(
                    children: [
                      Expanded(flex: 8, child: Image.asset('assets/tool.png')),
                      Text(
                        "Plumber",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                      Spacer(),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: MediaQuery.of(context).size.width * .3,
                  height: MediaQuery.of(context).size.height * .18,
                ),
              ),
              Spacer(),
            ],
          ),
          Spacer(),
          Row(
            children: [
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {},    
                child: Container(
                  child: Column(
                    children: [
                      Expanded(flex: 8, child: Image.asset('assets/saw.png')),
                      Text(
                        "Carpenter",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                      Spacer(),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: MediaQuery.of(context).size.width * .3,
                  height: MediaQuery.of(context).size.height * .18,
                ),
              ),
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {},
                child: Container(
                  child: Column(
                    children: [
                      Expanded(flex: 8, child: Image.asset('assets/knife.png')),
                      Text(
                        "Gardener",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                      Spacer(),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: MediaQuery.of(context).size.width * .3,
                  height: MediaQuery.of(context).size.height * .18,
                ),
              ),
              Spacer(),
            ],
          ),
          Spacer(),
          Expanded(
            flex: 9,
            child: Container(),
          ),
        ],
      ),
    );
  }
}
