import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/dashboard_page.dart';
import 'package:homeservice_ashlyn/piliaraw_page.dart';
import 'package:homeservice_ashlyn/profile_page.dart';

class Where_page extends StatefulWidget {
  const Where_page({super.key});

  @override
  State<Where_page> createState() => _Where_pageState();
}

class _Where_pageState extends State<Where_page> {
  bool button1 = false;
  bool button2 = false;
  bool button3 = false;
  bool button4 = false;

  bool bedcount1 = false;
  bool bedcount2 = false;
  bool bedcount3 = false;

  bool bathroom1 = false;
  bool bathroom2 = false;
  bool bathroom3 = false;

  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new_outlined,
                    size: 30,
                  ),
                ),
                Spacer(),
                Text("Cleaning", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
                Spacer(),IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.account_circle_outlined,
                    size: 40,
                  ),
                ),
              ],
            ),
          ),
          Text("Where do you want to cleaned?", style: TextStyle(fontWeight: FontWeight.bold),),
          Spacer(),
          //LivingRoom Buttom
          Row(
            children: [
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {
                  setState(() {
                    button1 = !button1;
                    if (button1 == true) {
                      count++;
                    } else {
                      count--;
                    }
                  });
                },
                child: Container(
                    child: button1
                        ? Row(
                            children: [
                              Image.asset('assets/living.png'),
                              Text(
                                "Living Room",
                                style: TextStyle(color: Colors.black),
                              ),
                              Spacer(),
                              Icon(
                                Icons.check,
                                size: 35,
                              ),
                            ],
                          )
                        : Row(
                            children: [
                              Image.asset('assets/living.png'),
                              Text(
                                "Living Room",
                                style: TextStyle(color: Colors.black),
                              ),
                              Spacer(),
                            ],
                          ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * .08),
              ),
              Spacer(),
            ],
          ),
          //end of LivingRoom Button
          Spacer(),
          //Kitchen Button
          Row(
            children: [
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {
                  setState(() {
                    button2 = !button2;
                    if (button2 == true) {
                      count++;
                    } else {
                      count--;
                    }
                  });
                },
                child: Container(
                    child: button2
                        ? Row(
                            children: [
                              Image.asset('assets/kitchen.png'),
                              Text(
                                "Kitchen",
                                style: TextStyle(color: Colors.black),
                              ),
                              Spacer(),
                              Icon(
                                Icons.check,
                                size: 35,
                              ),
                            ],
                          )
                        : Row(
                            children: [
                              Image.asset('assets/kitchen.png'),
                              Text(
                                "Kitchen",
                                style: TextStyle(color: Colors.black),
                              ),
                              Spacer(),
                            ],
                          ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * .08),
              ),
              Spacer(),
            ],
          ),
          //end of Kitchen Button
          Spacer(),
          //BedRoom Button
          Row(
            children: [
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {
                  setState(() {
                    button3 = !button3;

                    if (button3 == true) {
                      count++;
                    } else {
                      count--;
                    }
                  });
                },
                child: Container(
                    child: button3
                        ? Column(
                            children: [
                              Row(
                                children: [
                              Image.asset('assets/bedroom.png'),
                                  Text(
                                    "Bedroom",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  Spacer(),
                                  Icon(Icons.check)
                                ],
                              ),
                              Container(
                                child: Column(
                                  children: [
                                    Text("How many Bedrooms?"),
                                    Row(
                                      children: [
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: bedcount1
                                                ? Colors.amber
                                                : Colors.yellow,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              bedcount1 = !bedcount1;

                                              if (bedcount1 == true) {
                                                bedcount2 = false;
                                                bedcount3 = false;
                                              }
                                            });
                                          },
                                          child: Text("1"),
                                        ),SizedBox(width: 10,),
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: bedcount2
                                                ? Colors.amber
                                                : Colors.yellow,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              bedcount2 = !bedcount2;
                                              if (bedcount2 == true) {
                                                bedcount1 = false;
                                                bedcount3 = false;
                                              }
                                            });
                                          },
                                          child: Text("2"),
                                        ),SizedBox(width: 10,),
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: bedcount3
                                                ? Colors.amber
                                                : Colors.yellow,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              bedcount3 = !bedcount3;

                                              if (bedcount3 == true) {
                                                bedcount1 = false;
                                                bedcount2 = false;
                                              }
                                            });
                                          },
                                          child: Text("3"),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        : Row(
                            children: [
                              Image.asset('assets/bedroom.png'),
                              Text(
                                "Bedroom",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: button3
                        ? MediaQuery.of(context).size.height * .15
                        : MediaQuery.of(context).size.height * .08),
              ),
              Spacer(),
            ],
          ),
          //end of BedRoom Button
          Spacer(),
          //Bathroom Button
          Row(
            children: [
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: () {
                  setState(() {
                    button4 = !button4;

                    if (button4 == true) {
                      count++;
                    } else {
                      count--;
                    }
                  });
                },
                child: Container(
                    child: button4
                        ? Column(
                            children: [
                              Row(
                                children: [
                              Image.asset('assets/bathtub.png'),
                                  Text(
                                    "Bathroom",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  Spacer(),
                                  Icon(Icons.check)
                                ],
                              ),
                              Container(
                                child: Column(
                                  children: [
                                    Text("How many Bedrooms?"),
                                    Row(
                                      children: [
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: bathroom1
                                                ? Colors.amber
                                                : Colors.yellow,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              bathroom1 = !bathroom1;

                                              if (bathroom1 == true) {
                                                bathroom2 = false;
                                                bathroom3 = false;
                                              }
                                            });
                                          },
                                          child: Text("1"),
                                        ),SizedBox(width: 10,),
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: bathroom2
                                                ? Colors.amber
                                                : Colors.yellow,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              bathroom2 = !bathroom2;
                                              if (bathroom2 == true) {
                                                bathroom1 = false;
                                                bathroom3 = false;
                                              }
                                            });
                                          },
                                          child: Text("2"),
                                        ),SizedBox(width: 10,),
                                        ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: bathroom3
                                                ? Colors.amber
                                                : Colors.yellow,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              bathroom3 = !bathroom3;

                                              if (bathroom3 == true) {
                                                bathroom1 = false;
                                                bathroom2 = false;
                                              }
                                            });
                                          },
                                          child: Text("3"),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        : Row(
                            children: [
                              Image.asset('assets/bathtub.png'),
                              Text(
                                "Bathroom",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: button4
                        ? MediaQuery.of(context).size.height * .15
                        : MediaQuery.of(context).size.height * .08),
              ),
              Spacer(),
            ],
          ),
          //end Bathroom Button
          Spacer(),
          Expanded(
            flex: 9,
            child: Container(),
          ),
          Row(
            children: [
              Expanded(
                flex: 8,
                child: Container(),
              ),
              FloatingActionButton(
                backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Schedule_page(),
                    ),
                  );
                },
                child: Center(
                  child: Row(
                    children: [
                      Spacer(),
                      Text(
                        "${count}",
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 15,
                        color: Colors.black,
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
              Spacer(),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}
