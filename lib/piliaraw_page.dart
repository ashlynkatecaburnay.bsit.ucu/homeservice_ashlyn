import 'dart:js_util';

import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/profile_page.dart';
import 'package:homeservice_ashlyn/tagalinis_page.dart';

class Schedule_page extends StatefulWidget {
  const Schedule_page({
    super.key,
  });

  @override
  State<Schedule_page> createState() => _Schedule_pageState();
}

class _Schedule_pageState extends State<Schedule_page> {
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();

  bool button1 = false;
  bool button2 = false;
  bool button3 = false;

  final List<String> month = <String>[
    "January",
    "Febuary",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new_outlined,
                    size: 30,
                  ),
                ),
                Spacer(),
                Text("Select Date and Time", style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
                Spacer(),IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.account_circle_outlined,
                    size: 40,
                  ),
                ),
              ],
            ),
          ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  month[selectedDate.month - 1] + " ",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${selectedDate.year}",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromRGBO(250, 151, 252, 1),
              ),
              height: MediaQuery.of(context).size.height * 0.35,
              width: MediaQuery.of(context).size.width * 0.85,
              child: CalendarDatePicker(
                initialDate: selectedDate,
                firstDate: DateTime(2001),
                lastDate: DateTime(2030),
                onDateChanged: (DateTime? value) {
                  if (value != null) {
                    setState(() {
                      selectedDate = value;
                    });
                  }
                },
              ),
            ),
            Text(
              "Time",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 8,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(10),
                      backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    ),
                    onPressed: () async {
                      final TimeOfDay? setTime = await showTimePicker(
                          context: context, initialTime: selectedTime);
                      if (setTime != null) {
                        setState(() {
                          selectedTime = setTime;
                        });
                      }
                    },
                    child: Text(
                      "${selectedTime.format(context)}",
                      style: TextStyle(fontSize: 40, color: Colors.black),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            Spacer(),
            Row(
              children: [
                Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: button1
                        ? Color.fromRGBO(176, 37, 178, 1)
                        : Color.fromRGBO(250, 151, 252, 1),
                  ),
                  onPressed: () {
                    setState(() {
                      button1 = !button1;
                      if (button1 == true) {
                        button2 = false;
                        button3 = false;
                      }
                    });
                  },
                  child: Text("No Repeat"),
                ),
                Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: button2
                        ? Color.fromRGBO(176, 37, 178, 1)
                        : Color.fromRGBO(250, 151, 252, 1),
                  ),
                  onPressed: () {
                    setState(() {
                      button2 = !button2;
                      if (button2 == true) {
                        button1 = false;
                        button3 = false;
                      }
                    });
                  },
                  child: Text("Weekly"),
                ),
                Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    backgroundColor: button3
                        ? Color.fromRGBO(176, 37, 178, 1)
                        : Color.fromRGBO(250, 151, 252, 1),
                  ),
                  onPressed: () {
                    setState(() {
                      button3 = !button3;
                      if (button3 == true) {
                        button1 = false;
                        button2 = false;
                      }
                    });
                  },
                  child: Text("Every Month"),
                ),
                Spacer(),
              ],
            ),
            Spacer(),
            Row(
              children: [
                Expanded(
                  flex: 8,
                  child: Container(),
                ),
                FloatingActionButton(
                    backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Service_page(),
                        ),
                      );
                    },
                    child: Icon(
                      Icons.arrow_forward_ios_outlined,
                      color: Colors.black,
                    )),
                Spacer(),
              ],
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
