import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/login_page.dart';

class Umpisa_page extends StatelessWidget {
  const Umpisa_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Spacer(),
            Text(
              "Home Service App",
              style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
            ),
            Text(
              "We Keep all things fresh",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Container(
              height: MediaQuery.of(context).size.height * .4,
              width: MediaQuery.of(context).size.width * .9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(250, 151, 252, .7),
                      blurRadius: 5,
                      offset: Offset(4, 6),
                    ),
                  ]),
              child: Column(
                children: [
                  Spacer(),
                  Row(
                    children: [
                      Spacer(),
                      Container(
                        child: Column(
                          children: [
                            Expanded(
                              flex: 8,
                              child: Image.asset('assets/clean.png')),
                              
                            Text("Cleaning", style: TextStyle(fontWeight: FontWeight.bold),),
                            Spacer(),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 151, 252, 1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.height * .18,
                      ),
                      Spacer(),
                      Container(
                        child: Column(
                          children: [
                            Expanded(
                              flex: 8,
                              child: Image.asset('assets/tool.png')),
                              
                            Text("Plumber", style: TextStyle(fontWeight: FontWeight.bold),),
                            Spacer(),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 151, 252, 1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.height * .18,
                      ),
                      Spacer(),
                    ],
                  ),
                  Spacer(),
                  Row(
                    children: [
                      Spacer(),
                      Container(
                        child: Column(
                          children: [
                            Expanded(
                              flex: 8,
                              child: Image.asset('assets/saw.png')),
                              
                            Text("Carpenter", style: TextStyle(fontWeight: FontWeight.bold),),
                            Spacer(),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 151, 252, 1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.height * .18,
                      ),
                      Spacer(),
                      Container(
                        child: Column(
                          children: [
                            Expanded(
                              flex: 8,
                              child: Image.asset('assets/knife.png')),
                              
                            Text("Gardener", style: TextStyle(fontWeight: FontWeight.bold),),
                            Spacer(),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 151, 252, 1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.height * .18,
                      ),
                      Spacer(),
                    ],
                  ),
                  Spacer(),
                ],
              ),
            ),
            Spacer(),
            Text(
              "A place for everything and",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              "everthing in its place",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Spacer(),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Color.fromRGBO(250, 151, 252, 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Login_page(),
                  ),
                );
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Get Started", style: TextStyle(color: Colors.black.withOpacity(.6)),),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
