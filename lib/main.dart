import 'package:flutter/material.dart';
import 'package:homeservice_ashlyn/dashboard_page.dart';
import 'package:homeservice_ashlyn/login_page.dart';
import 'package:homeservice_ashlyn/piliaraw_page.dart';
import 'package:homeservice_ashlyn/profile_page.dart';
import 'package:homeservice_ashlyn/rehistro_pahina.dart';
import 'package:homeservice_ashlyn/tagalinis_page.dart';
import 'package:homeservice_ashlyn/umpisa_page.dart';
import 'package:homeservice_ashlyn/where_daw_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Umpisa_page(),
    );
  }
}